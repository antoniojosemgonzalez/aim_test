<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id')->autoIncrement();
            $table->string('name')->nullable(false);
            $table->string('email', 100)->nullable(false);
            $table->smallInteger('telephone')->nullable();
            $table->string('address');
            $table->boolean("delete")->default(0);
            $table->timestamp("deleted_at")->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
