@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ isset($isEditMode)?"Edit client":"New client" }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                    @endif
                    <!-- Small modal -->
                        <div id="modalSuccessful" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <p class="modal-title">Data save successful.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="result" style="display: none" class="alert alert-success" role="alert">
                        </div>
                        <form name="frmClients" method="POST" action="{{ route('home') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ isset($data->name)?$data->name:"" }}" autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ isset($data->email)?$data->email:"" }}">

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="telephone"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Telephone') }}</label>

                                <div class="col-md-6">
                                    <input id="telephone" type="tel" maxlength="8"
                                           class="form-control{{ $errors->has('telephone') ? ' is-invalid' : '' }}"
                                           name="telephone" value="{{ isset($data->telephone)?$data->telephone:"" }}">

                                    @if ($errors->has('telephone'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control"
                                           name="address" value="{{ isset($data->address)?$data->address:"" }}">
                                </div>
                            </div>

                            <div>
                                <div class="col-md-6">
                                    <input id="isEditMode" type="text" hidden
                                           name="isEditMode" value="{{ isset($isEditMode)?$isEditMode:0 }}">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Guardar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("form[name=frmClients]").submit(function (e) {
                e.preventDefault();
                e.stopPropagation();

                var name = $('#name').val();
                var email = $('#email').val();
                var telephone = $('#telephone').val();
                var address = $('#address').val();
                var isEditMode = parseInt($('#isEditMode').val());
                var error = false;


                $(".error").remove();
                $('input').removeClass('is-invalid');

                if (name.length < 1) {
                    $('#name').after('<span class="error"><strong>This field is required</strong></span>');
                    $('#name').addClass('is-invalid');
                    error = true;
                }
                if (email.length < 1) {
                    $('#email').after('<span class="error"><strong>This field is required</strong></span>');
                    $('#email').addClass('is-invalid');
                    error = true;
                } else {
                    var regEx = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    var validEmail = regEx.test(email);
                    if (!validEmail) {
                        $('#email').after('<span class="error"><strong>Enter a valid email</strong></span>');
                        $('#email').addClass('is-invalid');
                        error = true;
                    }
                }
                if (telephone.length < 8) {
                    $('#telephone').after('<span class="error"><strong>This field is required</strong></span>');
                    $('#telephone').addClass('is-invalid');
                    error = true;
                } else {
                    var regEx = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{2})/;
                    var validTelephone = regEx.test(telephone);
                    if (!validTelephone) {
                        $('#telephone').after('<span class="error"><strong>Enter a valid Telephone</strong></span>');
                        $('#telephone').addClass('is-invalid');
                        error = true;
                    }
                }
                if (address.length < 1) {
                    $('#address').after('<span class="error"><strong>This field is required</strong></span>');
                    $('#address').addClass('is-invalid');
                    error = true;
                }
                $(".error").addClass("invalid-feedback");

                console.log(error);
                if (!error) {

                    var dataSend =
                    {
                        'name': name,
                        'email': email,
                        'telephone': telephone,
                        'address': address,
                        _token: '{{csrf_token()}}'
                    };

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    if (isEditMode === 0) {
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            contentType: 'application/json',
                            url: '{{ url('client') }}',
                            data: JSON.stringify(dataSend),
                            success: function (data) {
                                $('form[name=frmClients]').trigger("reset");
                                $('#modalSuccessful').modal();
                            },
                            error: function (xhr) {
                                var err = JSON.parse(xhr.responseText);
                                if (err.error == "email_exists") {
                                    $('#email').after('<span class="error"><strong>The email entered already exists</strong></span>');
                                    $('#email').addClass('is-invalid');
                                    $(".error").addClass("invalid-feedback");
                                }
                            }
                        });
                    } else {
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            contentType: 'application/json',
                            url: '{{ url('client/edit/' . $data->id) }}',
                            data: JSON.stringify(dataSend),
                            success: function (data) {
                                window.location = "/home"
                            },
                            error: function (xhr) {
                            }
                        });
                    }
                }

                return true;
            });
        })
        ;
    </script>
@endsection
