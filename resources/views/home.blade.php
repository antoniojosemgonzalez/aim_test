@extends('layouts.app')

@section('css')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <style>
        .btnDelete {
            margin-left: 15px;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                <h3>Clients</h3>
                            </div>
                            <div class="col-md-2">
                                <a href="{{ url('/client') }}" class="btn btn-success">Add Client</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        Warning
                                    </div>
                                    <div class="modal-body">
                                        Are you sure you want to take this action?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                        </button>
                                        <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                                                data-dismiss="modal">Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-striped table-bordered"
                               id="clientList">
                            <thead>
                            <tr class="bg-primary!important">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $u)
                                <tr>
                                    <th>{{ $u->name }}</th>
                                    <th>{{ $u->email }}</th>
                                    <td>{{ $u->telephone }}</td>
                                    <td>{{ $u->address }}</td>
                                    <td>
                                        <a class="btn btn-primary" role="button" aria-pressed="true"
                                           href="{{ url('/client/edit/'.$u->id) }}">Edit</a>
                                        <Button id="btnDelete" class="btn btn-warning btnDelete"
                                                data-href="{{ url('/client/delete/'.$u->id) }}">
                                            Delete
                                        </Button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#clientList').DataTable();
            $('#btnDelete').click(function () {
                $('#confirm-delete').modal();
            });
            $('#btnConfirmDelete').click(function () {
                var url = $('#btnDelete').data('href');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: url,
                    success: function (data) {
                        window.location = "/home"
                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            });
        });
    </script>
@endsection
