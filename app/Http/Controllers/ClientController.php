<?php

namespace App\Http\Controllers;

use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    public function index()
    {
        $model = new Client();
        $model->id = 0;
        
        return view('client', ['data' => $model]);
    }

    public function create(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        $client = new Client();
        /*$email = $client->select('email')->where('email', $content['email'])->first();
        if ($email != null) {
            return response()->json(array('success' => false, 'error' => "email_exists"), 400);
        }*/

        $client->name = $content['name'];
        $client->email = $content['email'];
        $client->telephone = $content['telephone'];
        $client->address = $content['address'];

        $client->save();

        return response()->json(array('success' => true, 'message' => 'Data save successful'), 200);
    }

    public function update(Request $request, $id)
    {
        $content = json_decode($request->getContent(), true);

        $client = Client::find($id);

        $client->name = $content['name'];
        $client->email = $content['email'];
        $client->telephone = $content['telephone'];
        $client->address = $content['address'];

        $client->save();

        return response()->json(array('success' => true, 'message' => 'Data save successful'), 200);
    }

    public function delete(Request $request, $id)
    {
        $client = Client::find($id);
        $client->delete = 1;
        $client->deleted_at = Carbon::now()->toDateTimeString();

        $client->save();
        return response()->json(array('success' => true, 'message' => 'Data deleted successful'), 200);
    }

    public function updateIndex($id)
    {
        $model = new Client();
        $data = $model->find($id);
        return view('client', ['data' => $data, "isEditMode" => true]);
    }
}
