<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class HomeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::user()->id;

        $uModel = new User();
        $u = $uModel->find($userId);
        if (!$u->active) {
            Auth::logout();
            return redirect()->to('/login')->with('status', 'The user is not active');
        }

        return $next($request);
    }
}
